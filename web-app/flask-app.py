# Developed by : Seyyed Ali Ayati
# Company : Iran University of Science & Technology | Computer Engineering College
# May 2018
# University Professor : Dr. Adel Rahmani

# Import modules
from flask import Flask, render_template, request, redirect
import sqlite3
from sqlite3 import Error

# Init web app and connect database
app = Flask(__name__)
try:
        conn = sqlite3.connect('books.db')
        cur = conn.cursor()
except Error as e:
        print(e)

# Variable "current" is for remembering page number
global current
current = 0

# Get data from database
global all_names
all_names = []
cur.execute("""SELECT BookName FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_names.append(i[0])

global all_links
all_links = []
cur.execute("""SELECT src FROM FarsiImageLinks""")
items = cur.fetchall()
for i in items:
	all_links.append(i[0])

global all_author
all_author = []
cur.execute("""SELECT Author FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_author.append(i[0])

global all_price
all_price = []
cur.execute("""SELECT Price FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_price.append(i[0])

global all_publisher
all_publisher = []
cur.execute("""SELECT Publisher FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_publisher.append(i[0])
 
global all_year
all_year = []
cur.execute("""SELECT Year FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_year.append(i[0])

global all_isbn
all_isbn = []
cur.execute("""SELECT ISBN FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_isbn.append(i[0])

global all_subject
all_subject = []
cur.execute("""SELECT subject FROM FarsiBookDetails""")
items = cur.fetchall()
for i in items:
	all_subject.append(i[0])

global all_section
all_section = []
cur.execute("""SELECT Section FROM FarsiBookLocations""")
items = cur.fetchall()
for i in items:
	all_section.append(i[0])

global all_hall
all_hall = []
cur.execute("""SELECT Hall FROM FarsiBookLocations""")
items = cur.fetchall()
for i in items:
	all_hall.append(i[0])

global all_corridor
all_corridor = []
cur.execute("""SELECT Corridor FROM FarsiBookLocations""")
items = cur.fetchall()
for i in items:
	all_corridor.append(i[0])

global all_standno
all_standno = []
cur.execute("""SELECT StandNo FROM FarsiBookLocations""")
items = cur.fetchall()
for i in items:
	all_standno.append(i[0])

# Root link "/" show 10 book in current page
@app.route("/")
def root():
	global all_names, current, all_links, all_author, all_price
	dic = [all_names[current:current+10], all_links[current:current+10], all_author[current:current+10]
	, all_price[current:current+10]]
	return render_template("index.html", data = dic)

# Detail link "/detail/a number in range(0, 10)" show details of each book
@app.route("/detail/<variable>")
def detail(variable):
	global current, all_links, all_publisher, all_year, all_isbn, all_subject, all_section, all_hall, all_corridor, all_standno, all_names, all_author
	ref = current + int(variable)
	data = [all_links[ref], all_names[ref], all_publisher[ref], all_year[ref], all_isbn[ref], all_subject[ref], 
	all_section[ref], all_hall[ref], all_corridor[ref], all_standno[ref], all_author[ref]]
	return render_template("subpage.html", Data = data)

# Next page link "/nextpage" show the next 10 books
@app.route("/nextpage")
def nextpage():
	global current
	current += 10
	return redirect("/")

# Previous page link "/previouspage" show the previous 10 books
@app.route("/previouspage")
def previouspage():
	global current
	current -= 10
	return redirect("/")

# Goto page link "/goto" get the page number from user and show its books 
@app.route("/goto", methods = ['POST', 'GET'])
def goto():
	global current
	if request.method == 'POST':
		result = request.form
		searched = str(result['page'])
		current = int(searched)*10
	return redirect("/")

# Is an page about me !
@app.route("/Company")
def company():
	return render_template('company.html')

# Run the application and then open your browser and open this url:
# localhost:5000/
if __name__ == '__main__':
   app.run(debug = True)
