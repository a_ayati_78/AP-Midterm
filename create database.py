# Developed by : Seyyed Ali Ayati
# Company : Iran University of Science & Technology | Computer Engineering College
# May 2018
# University Professor : Dr. Adel Rahmani

# Import modules
import sqlite3
from sqlite3 import Error

try:
	# If we don't have books.db it will be created else it will be connected.
	conn = sqlite3.connect("books.db")
	print('connected :', sqlite3.version)
except Error as e:
	print(e)

# Create cursor to perform sql
c = conn.cursor()

# Create table #1
c.execute("""CREATE TABLE IF NOT EXISTS FarsiBookDetails (
	BookID	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	BookName	TEXT,
	ISBN	TEXT,
	Author	TEXT,
	Publisher	TEXT,
	Price	INTEGER,
	Year	INTEGER,
	subject	TEXT
);""")

# Create table #2
c.execute("""CREATE TABLE IF NOT EXISTS FarsiBookLocations (
	LocationID	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	Section	TEXT,
	Hall	TEXT,
	Corridor	TEXT,
	StandNo	INTEGER
);""")

# Create table #3
c.execute("""CREATE TABLE IF NOT EXISTS FarsiImageLinks (
	LinkID	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	src	TEXT
);""")

# Save changes to books.db
conn.commit()

# Close database
conn.close()
print ("Database created !")