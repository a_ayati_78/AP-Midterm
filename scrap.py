# Developed by : Seyyed Ali Ayati
# Company : Iran University of Science & Technology | Computer Engineering College
# May 2018
# University Professor : Dr. Adel Rahmani

# Import modules
import urllib2, sqlite3
from bs4 import BeautifulSoup


class Scraper():
	def __init__(self, url):
		# Get page url for scrap.
		print 'Scraper Started.'
		self.url = url
	
	def connect(self):
		# Connect to given url and make its soup !
		try:
			print 'Getting page info.'
			self.page = urllib2.urlopen(self.url)
			self.soup = BeautifulSoup(self.page, 'html.parser')
			self.get_names()
		except:
			# If you don't have internet connection this message will show.
			print 'Please check your internet connection and try again...'
			return None

	def get_names(self):
		# Get book names.
		names = self.soup.findAll('a', attrs={'class':'book-detail-link'})
		self.title = []
		for name in names:
			name = name.text.strip()
			self.title.append(name)
	
	def get_details(self):
		# Get book details.
		data = self.soup.findAll('div', attrs={'class':'col-sm-6'})
		self.details = []
		self.isbns = []
		self.author = []
		self.publisher = []
		self.price = []
		self.issueYear = []
		self.subject = []
		for span in self.soup.select("div.col-sm-6 span"):
			try:
				s = span.text.strip()
				self.details.append(s)
			except:
				print 'error !'

		for i in range(0, len(self.details)-5, len(self.details)//10):
			self.isbns.append(self.details[i:i+7][0]) 
			self.author.append(self.details[i:i+7][1])
			self.publisher.append(self.details[i:i+7][2])
			self.price.append(self.details[i:i+7][3])
			self.issueYear.append(self.details[i:i+7][4])
			self.subject.append(self.details[i:i+7][5])
		
		# Init prices and issue year.
		for i in range(len(self.price)):
			self.price[i] = int(self.price[i][:len(self.price[i])-5].strip().replace(',', ''))
		
		for i in range(len(self.issueYear)):
			self.issueYear[i] = int(self.issueYear[i])
	
	def get_locations(self):
		# Get book locations.
		self.details = []
		self.sections = []
		self.halls = []
		self.corridors = []
		self.stand_no = []

		for span in self.soup.select("div.col-sm-3 span"):
			try:
				s = span.text.strip()
				self.details.append(s)
			except:
				print 'error !'
			
		
		for i in range(0, len(self.details)-3, len(self.details)//10):
			self.sections.append(self.details[i:i+4][0]) 
			self.halls.append(self.details[i:i+4][1])
			self.corridors.append(self.details[i:i+4][2])
			self.stand_no.append(self.details[i:i+4][3])
		
		# Some don't have corridors so I choose "-" istead of none.
		for i in range(len(self.corridors)):
			if len(self.corridors[i]) == 0:
				self.corridors[i] = "-"

	def get_image_links(self):
		self.image_src = []
		images = self.soup.findAll('img')
		for img in images:
			img = img.get('src').strip()
			self.image_src.append(img)
		self.image_src.remove(self.image_src[0])
		
		while len(self.image_src) != 10:
			self.image_src.insert(0, None)

	def save_to_database(self):
		# Connect to "books.db" and save data.
		print 'Coonecting to Database...'
		DB = sqlite3.connect('books.db')
		cursor = DB.cursor()
		for i in range(len(self.title)):
			cursor.execute("""INSERT INTO FarsiBookDetails (BookName, ISBN, Author, Publisher, Price, Year, subject) VALUES(?, ?, ?, ?, ?, ?, ?)""", 
				(self.title[i], self.isbns[i], self.author[i], self.publisher[i], self.price[i], self.issueYear[i], self.subject[i]))
			cursor.execute("""INSERT INTO FarsiBookLocations (Section, Hall, Corridor, StandNo) VALUES(?, ?, ?, ?)""", (self.sections[i], self.halls[i], self.corridors[i], self.stand_no[i]))
			cursor.execute("""INSERT INTO FarsiImageLinks(src) VALUES(?)""", [self.image_src[i]])
			DB.commit()
		print 'Data Seved to Database !'
		DB.close()


	def test(self):
		# A junk function for testing.
		print self.image_src
		print len(self.image_src)
	
for page in range(1, 1001):
	print 'Scrap Page :%i'%page
	url = 'https://www.tibf.ir/en/book?bookLang=fa&page=%s'%str(page)
	instance = Scraper(url)
	instance.connect()
	instance.get_names()
	instance.get_details()
	instance.get_locations()	
	instance.get_image_links()
	#instance.test()
	instance.save_to_database()